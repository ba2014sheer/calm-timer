# calm-timer

Calm Timer is a simple timer application with some calm sounds to choose from

![screenshot](ct-screenshot.png)


## Installation

There are no installation packages but it's simple to install by following these steps:

* Install Python 3.x
  * (On Windows: Download the Python 3.x installation package for your platform: https://www.python.org/downloads/)
* You may need to upgrade pip before continuing: On the command line: `pip install --upgrade pip` (On Ubuntu use `pip3` and `sudo -H`)
* On the command line: `pip install -r requirements.txt` (On Ubuntu use `pip3` and `sudo -H`)
* Download the project files from GitHub, by clicking on the "Clone or download" button and then "Download ZIP"
* Unzip the downloaded file
* (When in the application directory) Start the application by running `python calm-timer.py` (On Ubuntu use `python3`)
