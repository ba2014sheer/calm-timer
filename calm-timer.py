#!/usr/bin/env python3
import ct.__main__

ct.__main__.start()

"""
Programming TODO: Add time left to window title (so that the time can be seen in the system tray)
Design TODO: Figuring out ways to show the alarm more clearly. Perhaps also removing the focus on the close button, so that the window is not accidentally closed when the user types
Design TODO: Maybe adding start and stop times, so that the user can track certain activities. Plus adding sliders for tracking what happens at the beginning and end
Programming TODO: Adding photo of a flower on the left side of the popup "notification" window

"""
